//
//  Waiting.swift
//  SlicerUI
//
//  Created by Francis Conlon on 21/01/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import SpriteKit
import GameplayKit

class Waiting: GKState {
    unowned let scene: GameScene
    
    init(scene: SKScene) {
        self.scene = scene as! GameScene
        super.init()
    }
    
    override func didEnter(from previousState: GKState?)
    {
        if previousState is WaitingToStart
        {
            scene.isUserInteractionEnabled = false
            var maxtime = TimeInterval(0)
            for i in 0..<scene.currentEnemies.count
            {
                let enemy = scene.currentEnemies[i]
                enemy.alpha = 0
                let rand = CGFloat.random(min: 0, max: 360)
                let randRad = rand.degreesToRadians()
                var vector = CGVector.init(angle: randRad)
                vector = vector * 20
                let emitterNode = SKEmitterNode(fileNamed: "TrailParticle.sks")!
                emitterNode.targetNode = scene
                enemy.addChild(emitterNode)
                
                var time = TimeInterval()
                if i > 0
                {
                    time = TimeInterval(i)
                    if time > maxtime
                    {
                        maxtime = time
                    }
                } else {
                    time = TimeInterval(0)
                }
                
                enemy.run(SKAction.sequence([SKAction.wait(forDuration: time), SKAction.fadeIn(withDuration: 0.2), SKAction.run {
                    enemy.physicsBody!.applyImpulse(vector)
                }]))
            }
            
            scene.afterDelay(maxtime, runBlock: {self.scene.isUserInteractionEnabled = true})
            
            for i in 0..<scene.currentEdgeEnemies.count
            {
                let enemy = scene.currentEdgeEnemies[i]
                var actions = [SKAction]()
                for point in 1..<scene.points.count
                {
                    let speed: CGFloat = 400.0
                    
                    let distance = scene.points[point - 1] - scene.points[point]
                    let distanceVector = CGVector(point: distance)
                    print(distance)
                    let time = distanceVector.length() / speed
                    let action = SKAction.move(to: scene.convert(scene.points[point], from: scene.shape), duration: TimeInterval(time))
                    actions.append(action)
                }
                //enemy.run(SKAction.repeatForever(SKAction.sequence(actions)))
                let follow = SKAction.follow(scene.shape.path!, asOffset: false, orientToPath: false, speed: 300)
                
                let distance = scene.points[0] - scene.shape.convert(enemy.position, to: scene)
                print("distance: \(distance)")
                let vector = CGVector(point: distance).length()
                enemy.position = scene.points[2]
                //enemy.run(SKAction.repeatForever(follow), withKey: "follow")
                enemy.run(SKAction.sequence([SKAction.wait(forDuration: TimeInterval(i)),SKAction.fadeIn(withDuration: 0.2), SKAction.repeatForever(follow)]), withKey: "follow")
                //enemy.alpha = 1
                
            }
        }
    }
    
    override func willExit(to nextState: GKState)
    {

    }
    
    override func isValidNextState(_ stateClass: AnyClass) -> Bool
    {
        return stateClass is Slicing.Type
    }
    
}

