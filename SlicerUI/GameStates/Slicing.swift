//
//  Slicing.swift
//  SlicerUI
//
//  Created by Francis Conlon on 21/01/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import SpriteKit
import GameplayKit

class Slicing: GKState {
    unowned let scene: GameScene
    
    init(scene: SKScene) {
        self.scene = scene as! GameScene
        super.init()
    }
    
    override func didEnter(from previousState: GKState?)
    {
        
    }
    
    override func willExit(to nextState: GKState)
    {
        //if nextState is Playing
        
        //let scale = SKAction.scale(to: 0, duration: 0.4)
        //scene.childNode(withName: GameMessageName)!.run(scale)
        
    }
    
    override func isValidNextState(_ stateClass: AnyClass) -> Bool {
        if stateClass is Waiting.Type || stateClass is GameOver.Type
        {
            return true
        } else {
            return false
        }
    }
    
}
