//
//  WaitingToStart.swift
//  SlicerUI
//
//  Created by Francis Conlon on 21/01/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import SpriteKit
import GameplayKit

class WaitingToStart: GKState {
    unowned let scene: GameScene
    
    init(scene: SKScene) {
        self.scene = scene as! GameScene
        super.init()
    }
    
    override func didEnter(from previousState: GKState?)
    {
        if previousState is GameOver
        {
            scene.points = scene.processRawPointData(rawData: scene.initialRawPointData)

        }
        
        let moveAction = SKAction.move(to: CGPoint(x: scene.scoreBackgroundNode.position.x, y: 1230), duration: 0.5)
        moveAction.timingMode = .easeOut
        scene.scoreBackgroundNode.run(moveAction)
        
        scene.messageLabel.setScale(0)
        print(scene.messageLabel.fontName)
        let scale = SKAction.scale(to: 1.2, duration: 0.4)
        let scaleDown = SKAction.scale(to: 0.9, duration: 0.2)
        let scaleUp = SKAction.scale(to: 1.1, duration: 0.3)
        let scaleRepeat = SKAction.repeatForever(SKAction.sequence([scaleDown, scaleUp]))
        scale.timingMode = .easeOut
        scaleUp.timingMode = .easeOut
        scaleDown.timingMode = .easeIn
        scene.messageLabel.run(SKAction.sequence([scale, scaleRepeat]))
    }
    
    override func willExit(to nextState: GKState)
    {
        //if nextState is Playing
        scene.messageLabel.removeAllActions()
        let scale = SKAction.scale(to: 0, duration: 0.4)
        scene.messageLabel.run(scale)
        
    }
    
    override func isValidNextState(_ stateClass: AnyClass) -> Bool {
        return stateClass is Waiting.Type
    }
    
}

