//
//  GameOver.swift
//  SlicerUI
//
//  Created by Francis Conlon on 21/01/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameOver: GKState {
    unowned let scene: GameScene
    
    init(scene: SKScene) {
        self.scene = scene as! GameScene
        super.init()
    }
    
    override func didEnter(from previousState: GKState?)
    {
        scene.physicsWorld.gravity = CGVector(dx: 0, dy: -10)
        for enemy in scene.currentEnemies
        {
            enemy.physicsBody!.velocity = CGVector.zero
            enemy.physicsBody?.restitution = 0.5
            print("enemy position: \(enemy.position)")
        }
        
        for enemy in scene.currentEdgeEnemies
        {
            let enemySpeed = enemy.physicsBody!.velocity
            enemy.removeAction(forKey: "follow")
        }
        
        let isWon = scene.isWon
        
        if isWon
        {
            scene.messageLabel.text = "You Win"
            SKTAudio.sharedInstance().playSoundEffect("game_win.wav")
        } else {
            scene.messageLabel.text = "You Lose"
            SKTAudio.sharedInstance().playSoundEffect("game_lose.wav")
        }
        scene.messageLabel.run(SKAction.scale(to: 1, duration: 0.4))
    }
    
    override func willExit(to nextState: GKState)
    {
        //if nextState is Playing
        
        //let scale = SKAction.scale(to: 0, duration: 0.4)
        //scene.childNode(withName: GameMessageName)!.run(scale)
        
    }
    
    override func isValidNextState(_ stateClass: AnyClass) -> Bool {
        return stateClass is WaitingToStart.Type
    }
    
}
