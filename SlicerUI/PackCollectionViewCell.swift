//
//  PackCollectionViewCell.swift
//  SlicerUI
//
//  Created by Francis Conlon on 25/01/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//


import UIKit

class PackCollectionViewCell: UICollectionViewCell
{
    
    
    @IBOutlet var percentageLabel: UILabel!
    @IBOutlet var lockedLabel: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    
    override func awakeFromNib() {
        
    }
}
