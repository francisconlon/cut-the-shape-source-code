//
//  LevelDataSource.swift
//  SlicerUI
//
//  Created by Francis Conlon on 24/01/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import UIKit
import CoreData

class LevelDataSource: NSObject, UICollectionViewDataSource
{
    var levels = NSMutableArray()
    var levelsCompleted: [NSManagedObject] = []

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return levels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let identifier = "LevelCollectionViewCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)  as! LevelCollectionViewCell
        
        let level = levels[indexPath.row] as! NSMutableDictionary
        cell.interiorLabel.text = String(level["Enemies"] as! Int)
        cell.edgeLabel.text = String(level["EdgeEnemies"] as! Int)
        if level["Completed"] as! Bool {
            cell.completed.alpha = 1
        } else {
            cell.completed.alpha = 0
        }
        
        if let name = level["Name"] as? String
        {
            let imageName = String("\(name).png")
            if let image = UIImage(named: imageName)
            {
                cell.levelPreview.image = image
                //cell.levelPreview.contentMode = .scaleAspectFit
            }
        }
        
        return cell
    }
}
