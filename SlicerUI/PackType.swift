import Foundation

enum PackType: Int, CustomStringConvertible {
    case Basic, Advanced, Extreme
    
    var description: String {
        switch self {
        case .Basic:
            return "Basic"
        case .Advanced:
            return "Advanced"
        case .Extreme:
            return "Extreme"
        }
    }
    
    var displayDescription: String {
        switch self {
        case .Basic:
            return "Basic Pack"
        case .Advanced:
            return "Advanced Pack"
        case .Extreme:
            return "Extreme Pack"
        }
    }
    
    
}
