//
//  Level.swift
//  SlicerUI
//
//  Created by Francis Conlon on 24/01/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import Foundation


class Level: NSObject, NSCoding
{
    var pack: String
    var level: Int
    var pointData: String
    var completed: Bool
    var edgeEnemies: Int
    var enemies: Int
    var name: String
    var target: Int
    
    init(pack: String, level: Int, pointData: String, completed: Bool, enemies: Int, edgeEnemies: Int, name: String, target: Int)
    {
        self.pack = pack
        self.level = level
        self.pointData = pointData
        self.completed = completed
        self.edgeEnemies = edgeEnemies
        self.enemies = enemies
        self.name = name
        self.target = target
    }
    

    func encode(with aCoder: NSCoder) {
        aCoder.encode(pack, forKey: "pack")
        aCoder.encode(level, forKey: "level")
        aCoder.encode(pointData, forKey: "pointData")
        aCoder.encode(completed, forKey: "completed")
        aCoder.encode(edgeEnemies, forKey: "edgeEnemies")
        aCoder.encode(enemies, forKey: "enemies")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(target, forKey: "target")
    }
    
    required init?(coder aDecoder: NSCoder) {
        pack = aDecoder.decodeObject(forKey: "pack") as! String
        level = aDecoder.decodeObject(forKey: "level") as! Int
        pointData = aDecoder.decodeObject(forKey: "pointData") as! String
        completed = aDecoder.decodeObject(forKey: "completed") as! Bool
        edgeEnemies = aDecoder.decodeObject(forKey: "edgeEnemies") as! Int
        enemies = aDecoder.decodeObject(forKey: "enemies") as! Int
        name = aDecoder.decodeObject(forKey: "name") as! String
        target = aDecoder.decodeObject(forKey: "target") as! Int
        
        super.init()
    }
}
