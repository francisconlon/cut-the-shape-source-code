//
//  GameScene.swift
//  SlicerUI
//
//  Created by Francis Conlon on 11/01/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import SpriteKit
import GameplayKit

let NoneCategory: UInt32 = 0b0
let ShapeEdgeCategory: UInt32 = 0b1
let EnemyCategory: UInt32 = 0b10
let SliceCategory: UInt32 = 0b100

class GameScene: SKScene
{
    var levelIndex: Int!
    var isWon = false
    var initialRawPointData: String!
    var enemies, edgeEnemies: Int!
    var messageLabel: SKLabelNode!
    var navController: UINavigationController!
    lazy var gameState: GKStateMachine = GKStateMachine(states: [WaitingToStart(scene: self),
                                                                 Waiting(scene: self),
                                                                 Slicing(scene: self),
                                                                 GameOver(scene: self)])
    var edgeEnemyHolder: SKSpriteNode!
    var shapeColour: UIColor = UIColor(red: 0.98, green: 0.63, blue: 0.09, alpha: 1.0)
    var initialShapeArea: CGFloat!
    var currentShapeArea: CGFloat! {
        didSet {
            if currentShapeArea < initialShapeArea
            {
                var percentage = currentShapeArea / initialShapeArea * 100
                percentage.round(.toNearestOrAwayFromZero)
                var text = "\(percentage)"
                if labelNode != nil {
                    labelNode.text = text
                    if percentage <= CGFloat(targetShapeArea)
                    {
                        labelNode.fontColor = SKColor.green
                    } else {
                        labelNode.fontColor = SKColor.red
                    }
                }
                
                if scoreNode != nil
                {
                    var x = (100 - percentage) / 100
                    var newWidth = scoreBackgroundNode.size.width * x
                    scoreNode.run(SKAction.resize(toWidth: newWidth, duration: 0.4))
                    
                    if percentage <= CGFloat(targetShapeArea)
                    {
                        scoreNode.color = SKColor.green
                        isWon = true
                        self.gameState.enter(GameOver.self)
                        
                    } else {
                        scoreNode.color = SKColor.red
                    }
                }
            }
        }
    }
    var targetShapeArea: Int!
    var labelNode: SKLabelNode!
    var scoreIndicator: SKShapeNode!
    var scoreIndicatorMaxWidth: CGFloat!
    
    var scoreNode: SKSpriteNode!
    var targetScoreNode: SKSpriteNode!
    var scoreBackgroundNode: SKSpriteNode!
    
    var sliceNode: SKShapeNode!
    var sliceStart: CGPoint! {
        didSet{
            if sliceStart == nil
            {
            } else {
                firstPoint = sliceStart
            }
        }
    }
    
    var sliceCurrent: CGPoint! {
        didSet{
            if sliceCurrent == nil
            {
            } else {
                if sliceCurrent.x > sliceStart.x
                {
                    firstPoint = sliceStart
                    secondPoint = sliceCurrent
                } else if sliceCurrent.x < sliceStart.x {
                    secondPoint = sliceStart
                    firstPoint = sliceCurrent
                }
            }
            var rise = (secondPoint.y - firstPoint.y)
            var run = (secondPoint.x - firstPoint.x)
            gradient = rise / run
            positiveNormal = CGVector(dx: -rise, dy: run)
            negativeNormal = CGVector(dx: rise, dy: -run)
            
            yIntercept = CGPoint(x: 0, y: (firstPoint.y - (gradient * firstPoint.x)))
            yInterceptPlus = CGPoint(x: size.width, y: ((gradient * size.width) + yIntercept.y))
        }
    }
    
    var sliceable = false
    
    var lineColor: SKColor {
        // convert point
        let convertedFirst = shape.convert(firstPoint, from: scene!)
        let convertedSecond = shape.convert(secondPoint, from: scene!)
        
        if shape.path!.contains(convertedFirst) || shape.path!.contains(convertedSecond)
        {
            sliceable = false
            return SKColor.red
        } else {
            sliceable = true
            return SKColor.green
        }
    }

    var firstPoint: CGPoint = CGPoint.zero
    var secondPoint: CGPoint = CGPoint.zero
    var yIntercept: CGPoint = CGPoint.zero
    var yInterceptPlus: CGPoint = CGPoint.zero
    var gradient: CGFloat!
    var positiveNormal: CGVector!
    var negativeNormal: CGVector!
    var slicePath: UIBezierPath!
    
    var points: [CGPoint]!
    var shapePath: CGMutablePath!
    var shape: SKShapeNode!
    var currentShapes = [SKShapeNode]()
    var currentEnemies = [SKShapeNode]()
    var currentEdgeEnemies = [SKShapeNode]()
    var scaledPoints: [CGPoint]!
    var shapeViewSize: CGSize!
    
    var backButton: SKLabelNode!
    
    override func sceneDidLoad()
    {

    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        switch gameState.currentState
        {
        case is WaitingToStart:
           return
        case is Waiting:

            if sliceNode == nil
            {
                let touch = touches.first!
                sliceStart = touch.location(in: self)
                
                sliceNode = SKShapeNode()
                sliceNode.fillColor = lineColor
                sliceNode.strokeColor = lineColor
                sliceNode.lineWidth = 10
            }

            gameState.enter(Slicing.self)
        case is Slicing:
            sliceNode.removeFromParent()
            sliceNode = nil
            gameState.enter(Waiting.self)
        case is GameOver:
            return
        default:
            break
        }
        
        
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        if sliceNode != nil
        {
            let touch = touches.first!
            let touchLocation = touch.location(in: self)
            sliceCurrent = touchLocation
            
            slicePath = UIBezierPath()
            slicePath.move(to: firstPoint)
            slicePath.addLine(to: secondPoint)
            print("Gradient: \(gradient)")
            sliceNode.removeFromParent()
            sliceNode = SKShapeNode(path: slicePath.cgPath)
            sliceNode.physicsBody = SKPhysicsBody(edgeFrom: firstPoint, to: secondPoint)
            sliceNode.physicsBody!.categoryBitMask = SliceCategory
            sliceNode.fillColor = lineColor
            sliceNode.fillTexture = SKTexture(imageNamed: "button.png")
            sliceNode.strokeColor = lineColor
            sliceNode.lineWidth = 0
            addChild(sliceNode)
            
            enumerateChildNodes(withName: "contactPoint", using: {(node, stop) in
                node.removeFromParent()
            })
            
            
            physicsWorld.enumerateBodies(alongRayStart: firstPoint, end: secondPoint, using: {(body, point, normal, stop) in
                if body.node?.name == "shape"
                {
                    let contactPoint = SKShapeNode(circleOfRadius: 5)
                    contactPoint.fillColor = SKColor.red
                    contactPoint.strokeColor = SKColor.red
                    contactPoint.position = point
                    contactPoint.name = "contactPoint"
                    self.addChild(contactPoint)
                }
            })
        }
    }
    
    func order(array: [CGPoint], toStartWith point: CGPoint) -> [CGPoint]
    {
        var finalArray = [CGPoint]()
        
        var firstIndex = array.firstIndex(of: point)!
        for i in firstIndex..<array.count
        {
            finalArray.append(array[i])
        }
        for i in 0..<firstIndex
        {
            finalArray.append(array[i])
        }
        
        return finalArray
    }
    
    func insert(newPoints: [CGPoint], into array: [CGPoint]) -> [CGPoint]
    {
        var finalArray = [CGPoint]()
        
        for i in 0..<array.count
        {
            // check if any contact points fall on line between previous and current point
            if i > 0
            {
                let previous = array[i-1]
                let current = array[i]
                var bigger = CGPoint()
                var smaller = CGPoint()
                if previous.x > current.x
                {
                    bigger = previous
                    smaller = current
                } else {
                    bigger = current
                    smaller = previous
                }
                // define line between previous and current point
                var lineGradient = (bigger.y - smaller.y)/(bigger.x - smaller.x)
                var lineYIntercept = CGPoint(x: 0, y: (current.y - (lineGradient * current.x)))
                
                let path = CGMutablePath()
                path.move(to: previous)
                path.addLine(to: current)
                
                for point in newPoints
                {
                    let distance = distanceFromPoint(p: point, toLineSegment: smaller, and: bigger)
                    if distance < 0.1
                    {
                        finalArray.append(point)
                    }
                }
            }
            finalArray.append(array[i])
        }
        
        return finalArray
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
    {

        
        switch gameState.currentState
        {
        case is WaitingToStart:
            gameState.enter(Waiting.self)
        case is Waiting:
            break
        case is Slicing:
            if sliceNode != nil
            {
                var contactPoints = [CGPoint]()
                
                enumerateChildNodes(withName: "contactPoint", using: {(node, stop) in
                    contactPoints.append(node.position)
                })
                
                
                if contactPoints.count > 0 && contactPoints.count % 2 == 0 && sliceable
                {
                    
                    // MARK: - Insert contact points
                    var pointsWithContacts = [CGPoint]()
                    var convertedContactPoints = [CGPoint]()
                    var pointsWithContactsWithEnemys = pointsWithContacts

                    for point in contactPoints
                    {
                        let p = shape.convert(point, from: scene!)
                        convertedContactPoints.append(p)
                    }
                    
                    pointsWithContacts = insert(newPoints: convertedContactPoints, into: points)
                    
                    var enemyPos = [CGPoint]()
                    for enemy in currentEdgeEnemies { enemyPos.append(enemy.position) }
                    pointsWithContactsWithEnemys = insert(newPoints: enemyPos, into: pointsWithContacts)
                    
                    // MARK: - end of insert points, Start of order so starts with contact
                    
                    var orderedPointsWithContacts = order(array: pointsWithContacts, toStartWith: convertedContactPoints.first!)
                    
                    if currentEdgeEnemies.count > 0 {
                     var orderedPointsWithContactsAndEnemies = order(array: pointsWithContactsWithEnemys, toStartWith: currentEdgeEnemies.first!.position)
                    }

                    
                    // MARK: - End of order array, start of creating arrays for use in paths
                    var numberOfBodies = contactPoints.count / 2 + 1
                    
                    var newShapePathArrays = [[CGPoint]]()
                    newShapePathArrays.append([CGPoint]())
                    newShapePathArrays[0].append(orderedPointsWithContacts.first!)
                    
                    var arrayIndex = 0
                    orderedPointsWithContacts.append(orderedPointsWithContacts.first!)
                    var flag: Int!
                    
                    //go through points in ordered array
                    for point in 1..<orderedPointsWithContacts.count
                    {
                        let pointToAdd = orderedPointsWithContacts[point]
                        
                        newShapePathArrays[arrayIndex].append(pointToAdd)
                        
                        if convertedContactPoints.contains(pointToAdd)
                        {
                            var array = [CGPoint]()
                            array.append(pointToAdd)
                            newShapePathArrays.append(array)
                            arrayIndex += 1
                        }
                        
                    }
                    newShapePathArrays.popLast()
                    
                    if numberOfBodies > 2
                    {
                        var complexArray = 0
                        for i in 0..<newShapePathArrays.count
                        {
                            let array = newShapePathArrays[i]
                            if array.contains(convertedContactPoints.first!) && array.contains(convertedContactPoints.last!)
                            {
                                complexArray = i
                            }
                        }
                        var even = true
                        if complexArray & 2 == 0
                        {
                            even = true
                        } else {
                            even = false
                        }
                         var arraysToRemove = [Int]()
                        
                        for i in 0..<newShapePathArrays.count
                        {
                           
                            
                            let iEven = i % 2
                            var isEven = false
                            if iEven == 0 {
                                isEven = true
                            } else {
                                isEven = false
                            }
                            
                            let array = newShapePathArrays[i]
                            
                            if ((even && isEven) || (!even && !isEven)) && (i != complexArray)
                            {
                                for point in array
                                {
                                    newShapePathArrays[complexArray].append(point)
                                }
                                arraysToRemove.append(i)
                            }
                        }
                        
                        for i in 0..<newShapePathArrays.count
                        {
                            if arraysToRemove.contains(i)
                            {
                                newShapePathArrays[i] = [CGPoint]()
                            }
                        }
                    }
                    
                    var finalShapePathArrays = [[CGPoint]]()
                    for i in 0..<newShapePathArrays.count
                    {
                        var array = newShapePathArrays[i]
                        if array.count != 0
                        {
                            array.append(array.first!)
                            finalShapePathArrays.append(array)
                        }
                    }
                    
                    var newShapePaths = [CGMutablePath]()
                    
                    for i in 0..<finalShapePathArrays.count
                    {
                        let path = CGMutablePath()
                        var currentArray = finalShapePathArrays[i]
                        currentArray.removeLast()
                        
                        var array = currentArray.removeDuplicates()
                        
                        path.move(to: array.first!)
                        for point in array
                        {
                            path.addLine(to: point)
                        }
                        path.addLine(to: currentArray.first!)
                        newShapePaths.append(path)
                    }
                    
                    
                    // MARK: - End of making paths, draw shapes
                    
                    enumerateChildNodes(withName: "contactPoint", using: {(node, stop) in
                        node.removeFromParent()
                        })
                    
                    currentShapes = [SKShapeNode]()
                    
                    for path in newShapePaths
                    {
                        path.closeSubpath()
                        let node = SKShapeNode(path: path)
                        node.fillColor = shapeColour
                        node.strokeColor = shapeColour
                        node.lineWidth = 1
                        node.position = shape.position
                        addChild(node)
                        node.physicsBody = SKPhysicsBody(polygonFrom: node.path!)
                        node.physicsBody!.collisionBitMask = 0
                        currentShapes.append(node)
                        let center = CGPoint(x: node.frame.midX, y: node.frame.midY)                        
                    }
                    
                    shape.removeFromParent()
                    var biggestShape: SKShapeNode!
                    var biggestShapeArea: CGFloat = 0.0
                    var biggestShapeIndex = 0
                                        
                    for i in 0..<currentShapes.count
                    {
                        var shape = currentShapes[i]
                        if shape.physicsBody!.area > biggestShapeArea
                        {
                            biggestShape = shape
                            biggestShapeArea = shape.physicsBody!.area
                            biggestShapeIndex = i
                        }
                    }
                                        
                    var edgePhysicsCheck = SKPhysicsBody()
                    
                    for currentShape in currentShapes
                    {
                        let shapePos = currentShape.convert(currentShape.position, from: scene!)
                        
                        if currentShape != biggestShape
                        {
                            currentShape.removeFromParent()
                            var newShape = SKShapeNode(path: currentShape.path!, centered: false)
                            newShape.fillColor = shapeColour
                            newShape.strokeColor = shapeColour
                            newShape.lineWidth = 1
                            addChild(newShape)
                            newShape.position = shape.position
                            let midPointBiggest = CGPoint(x: biggestShape.path!.boundingBoxOfPath.midX, y: biggestShape.path!.boundingBoxOfPath.midY)
                            let midPointCurrent = CGPoint(x: currentShape.path!.boundingBoxOfPath.midX, y: currentShape.path!.boundingBoxOfPath.midY)
                            
                            let difference = midPointCurrent - midPointBiggest
                            var vector = CGVector(point: difference).normalized()
                            vector = vector * 100
                                                        
                            let moveAction = SKAction.move(by: vector, duration: 0.6)
                            moveAction.timingMode = .easeOut
                            newShape.run(SKAction.sequence([SKAction.wait(forDuration: 0.2),moveAction, SKAction.fadeOut(withDuration: 0.2), SKAction.removeFromParent()]))
                        } else if currentShape == biggestShape {
                            currentShapeArea = currentShape.physicsBody!.area
                            shape = currentShape
                            shape.name = "shape"
                            shape.physicsBody = SKPhysicsBody(edgeLoopFrom: shape.path!)
                            shape.physicsBody!.collisionBitMask = 0
                            shape.physicsBody!.categoryBitMask = ShapeEdgeCategory
                            shape.physicsBody!.contactTestBitMask = EnemyCategory
                            
                            points = finalShapePathArrays[biggestShapeIndex]

                        }

                    }
                    
                    for enemy in currentEdgeEnemies
                    {
                        
                        var deadEnemiesPositions = [CGPoint]()
                        
                        scene!.childNode(withName: "bigShape")?.removeFromParent()
                        
                        var bigShape = shape.copy() as! SKShapeNode
                        bigShape.fillColor = .clear
                        bigShape.strokeColor = .clear
                        bigShape.scaleAsPoint = CGPoint(x: 1.01, y: 1.01)
                        bigShape.name = "bigShape"
                        addChild(bigShape)
                        
                        print(edgeEnemyHolder.convert(enemy.position, to: bigShape))
                        if bigShape.path!.contains(edgeEnemyHolder.convert(enemy.position, to: bigShape))
                        {
                            let array = finalShapePathArrays[biggestShapeIndex]
                            let arrayI = insert(newPoints: [enemy.position], into: array)
                            let arrayO = order(array: arrayI, toStartWith: enemy.position)
                            var arrayZ = arrayO.removeDuplicates()
                            arrayZ.append(arrayZ.first!)
                            let path = CGMutablePath()
                            path.move(to: arrayZ.first!)
                            for point in 1..<arrayZ.count
                            {
                                path.addLine(to: arrayZ[point])
                            }
                            enemy.removeAction(forKey: "follow")
                            enemy.run(SKAction.repeatForever(SKAction.follow(path, asOffset: false, orientToPath: false, speed: 300)), withKey: "follow")
                        } else {
                            enemy.run(SKAction.sequence([SKAction.fadeOut(withDuration: 0.3), SKAction.removeFromParent()]))
                            deadEnemiesPositions.append(enemy.position)
                        }
                        
                        var enemiesArr = [SKShapeNode]()
                        
                        for enemy in currentEdgeEnemies
                        {
                            if !deadEnemiesPositions.contains(enemy.position)
                            {
                                enemiesArr.append(enemy)
                            }
                        }
                        currentEdgeEnemies = enemiesArr
                        
                    }
                    
                    scene!.childNode(withName: "bigShape")?.removeFromParent()
                    
                    for i in 0..<contactPoints.count
                    {
                        if i % 2 == 0
                        {
                            let emitterNode = SKEmitterNode(fileNamed: "SliceParticle.sks")!
                            addChild(emitterNode)
                            emitterNode.zPosition = 5
                            emitterNode.position = contactPoints[i]
                            emitterNode.targetNode = scene
                            let moveAction = SKAction.move(to: contactPoints[i+1], duration: 0.2)
                            moveAction.timingMode = .easeInEaseOut
                            emitterNode.run(SKAction.sequence([moveAction, SKAction.wait(forDuration: 1),SKAction.removeFromParent()]))
                        }
                    }
                    
                    currentShapes = [SKShapeNode]()
                    currentShapes.append(biggestShape)
                    
                    SKTAudio.sharedInstance().playSoundEffect("slice.wav")
                    
                }
                
            

            
            sliceNode.removeFromParent()
            sliceNode = nil
            sliceStart = nil
            sliceCurrent = nil
            gameState.enter(Waiting.self)
        }
        case is GameOver:
            // Present new
            let vc = navController.topViewController as! GameViewController
            vc.playedLevels = vc.playedLevels + 1
            if vc.playedLevels >= 1
            {
                vc.presentAdvert()
            }
            
            if isWon {
                let lc = navController.topViewController as! GameViewController
                print(levelIndex)
                var level = lc.levelDataSource.levels[levelIndex] as! NSMutableDictionary
                level.setValue(true, forKey: "Completed")
                lc.levelDataSource.levels[levelIndex] = level
                print(lc.levelDataSource.levels.write(toFile: lc.fileURL.path, atomically: false))
                print(lc.levelDataSource.levels.count - 1, levelIndex!)
                if lc.levelDataSource.levels.count - 1 == levelIndex!
                {
                    navController.popViewController(animated: true)
                    return
                }
                levelIndex += 1
            }
            print(levelIndex)
            let nextLevel = vc.levelDataSource.levels[levelIndex] as! NSMutableDictionary
            let newScene = GameScene(fileNamed: "GameScene.sks")!
            newScene.size = scene!.size
            newScene.scaleMode = .aspectFill
            newScene.shapeViewSize = shapeViewSize
            newScene.navController = navController
            newScene.initialRawPointData = nextLevel["Points"] as! String
            newScene.targetShapeArea = nextLevel["Target"] as! Int
            newScene.enemies = nextLevel["Enemies"] as! Int
            newScene.edgeEnemies = nextLevel["EdgeEnemies"] as! Int
            newScene.levelIndex = levelIndex
            newScene.backgroundColor = self.backgroundColor
            
            
            
            let reveal = SKTransition.flipHorizontal(withDuration: 0.5)
            
            run(SKAction.sequence([SKAction.wait(forDuration: 1), SKAction.run {
                self.scene!.view!.presentScene(newScene)
            }]))
            
        default:
            break
        }
        let touchLocation = touches.first!.location(in: self)
        
        if backButton.contains(touchLocation)
        {
            print(navController.children.count)
            SKTAudio.sharedInstance().playSoundEffect("button_press.wav")
            navController.popViewController(animated: true)
        }
    }
    
    override func update(_ currentTime: TimeInterval)
    {

    }
    
    func setupLabel()
    {

        scoreBackgroundNode = scene!.childNode(withName: "ScoreBackground") as! SKSpriteNode
        scoreNode = scoreBackgroundNode.childNode(withName: "Score") as! SKSpriteNode
        targetScoreNode = scoreBackgroundNode.childNode(withName: "Indicator") as! SKSpriteNode
        backButton = scene!.childNode(withName: "Back") as! SKLabelNode
        messageLabel = scene!.childNode(withName: "MessageLabel") as! SKLabelNode
        print(messageLabel.fontName)
        var targetPos = scoreBackgroundNode.size.width
        targetPos = (targetPos * CGFloat(100 - targetShapeArea)) / 100
        targetScoreNode.position.x = targetPos
        scoreNode.size.width = 1
    }
    
    override func didMove(to view: SKView)
    {
        //////
        points = [CGPoint]()
        points = processRawPointData(rawData: initialRawPointData)
        
        backButton = SKLabelNode(text: "Back")
        backButton.fontSize = 100
        ////
        shapePath = CGMutablePath()
        shapePath.move(to: points[0])
        for point in 1..<points.count
        {
            shapePath.addLine(to: points[point])
        }
        ////
        shape = SKShapeNode(path: shapePath, centered: false)
        shape.position = CGPoint(x: size.width/2, y: size.height/2)
        shape.lineWidth = 1
        shape.fillColor = shapeColour
        shape.strokeColor = shapeColour
        //shape.fillTexture = SKTexture(imageNamed: "shape.png")
        shape.name = "shape"
        addChild(shape)
        ////
        var areaPhysics = SKPhysicsBody(polygonFrom: shape.path!)
        initialShapeArea = areaPhysics.area
        currentShapeArea = areaPhysics.area
        
        let physics = SKPhysicsBody(edgeLoopFrom: shape.path!)
        //let physics = SKPhysicsBody(polygonFrom: shape.path!)
        physics.categoryBitMask = ShapeEdgeCategory
        physics.contactTestBitMask = EnemyCategory
        physics.collisionBitMask = EnemyCategory
        physics.friction = 0.0
        print("Area enclosed: \(physics.area)")
        shape.physicsBody = physics
        
        physicsWorld.gravity = CGVector.zero
        physicsWorld.contactDelegate = self
        
        
        setupLabel()
        
        setupBouncyBoys()
        setupEdgyBoys()
        
        let backgroundEmitter = SKEmitterNode(fileNamed: "BackgroundParticle.sks")!
        backgroundEmitter.position = CGPoint(x: scene!.size.width/2, y: scene!.size.height/2)
        addChild(backgroundEmitter)
        
        gameState.enter(WaitingToStart.self)
    }
    
    func setupEdgyBoys()
    {
        edgeEnemyHolder = SKSpriteNode()
        edgeEnemyHolder.anchorPoint = CGPoint.zero
        edgeEnemyHolder.position = CGPoint(x: 0, y: 0)
        addChild(edgeEnemyHolder)
        
        var edgyCopy = edgeEnemies!
        while edgyCopy > 0
        {
            var edgeEnemy = SKShapeNode(circleOfRadius: 15)
            edgeEnemy.fillColor = UIColor(red: 0.984, green: 0.263, blue: 0.09, alpha: 1)
            edgeEnemy.strokeColor = SKColor.magenta
            
            edgeEnemyHolder.position = shape.convert(CGPoint.zero, to: scene!)
            edgeEnemyHolder.addChild(edgeEnemy)
            edgeEnemy.zPosition = 3
            edgeEnemy.position.x -= 15
            currentEdgeEnemies.append(edgeEnemy)

            edgeEnemy.alpha = 0
            print(edgeEnemy.position)
            
            edgeEnemy.physicsBody = SKPhysicsBody(circleOfRadius: 15)
            edgeEnemy.physicsBody!.categoryBitMask = EnemyCategory
            edgeEnemy.physicsBody!.contactTestBitMask = ShapeEdgeCategory | SliceCategory
            edgeEnemy.physicsBody!.collisionBitMask = 0
            edgeEnemy.physicsBody!.isDynamic = true
            edgeEnemy.physicsBody!.affectedByGravity = false
            
            edgyCopy -= 1
        }
    }
    
    func setupBouncyBoys()
    {
        var enemyCopy = enemies!
        print(enemyCopy)
        while enemyCopy > 0
        {
            var enemy = SKShapeNode(circleOfRadius: 15)
            enemy.fillColor = UIColor(red: 0.204, green: 0.545, blue: 0.675, alpha: 1)
            enemy.strokeColor = SKColor.cyan
            enemy.alpha = 0
            addChild(enemy)
            currentEnemies.append(enemy)
            
            var enemyPosPoints = [CGPoint]()
            physicsWorld.enumerateBodies(alongRayStart: CGPoint(x: 0, y: size.height/2),
                                         end: CGPoint(x: size.width, y: size.height/2),
                                         using: {(body, point, normal, stop) in
                                            if enemyPosPoints.count < 2
                                            {
                                                enemyPosPoints.append(point)
                                            }
            })
            print(enemyPosPoints.enumerated())
            
            let pointOne = enemyPosPoints[0]
            let pointTwo = enemyPosPoints[1]
            let midpoint = (pointTwo + pointOne) / 2
            enemy.position = midpoint
            
            enemy.zPosition = 3
            enemy.physicsBody = SKPhysicsBody(circleOfRadius: 15)
            enemy.physicsBody!.categoryBitMask = EnemyCategory
            enemy.physicsBody!.collisionBitMask = ShapeEdgeCategory
            enemy.physicsBody!.contactTestBitMask = SliceCategory
            enemy.physicsBody!.restitution = 1
            enemy.physicsBody!.friction = 0
            enemy.physicsBody!.allowsRotation = false
            enemy.physicsBody!.linearDamping = 0.0
            enemy.physicsBody!.angularDamping = 0.0
            
            enemyCopy -= 1
        }
    }
    
    /* Distance from a point (p1) to line l1 l2 */
    func distanceFromPoint(p: CGPoint, toLineSegment v: CGPoint, and w: CGPoint) -> CGFloat {
        let pv_dx = p.x - v.x
        let pv_dy = p.y - v.y
        let wv_dx = w.x - v.x
        let wv_dy = w.y - v.y
        
        let dot = pv_dx * wv_dx + pv_dy * wv_dy
        let len_sq = wv_dx * wv_dx + wv_dy * wv_dy
        let param = dot / len_sq
        
        var int_x, int_y: CGFloat /* intersection of normal to vw that goes through p */
        
        if param < 0 || (v.x == w.x && v.y == w.y) {
            int_x = v.x
            int_y = v.y
        } else if param > 1 {
            int_x = w.x
            int_y = w.y
        } else {
            int_x = v.x + param * wv_dx
            int_y = v.y + param * wv_dy
        }
        
        /* Components of normal */
        let dx = p.x - int_x
        let dy = p.y - int_y
        
        return sqrt(dx * dx + dy * dy)
    }
    
    func randomDirection() -> CGFloat
    {
        let speedFactor: CGFloat = 10
        if randomFloat(from: 0.0, to: 100.0) >= 50
        {
            return -speedFactor
        } else {
            return speedFactor
        }
    }
    
    func randomFloat(from: CGFloat, to: CGFloat) -> CGFloat
    {
        let rand: CGFloat = CGFloat(Float(arc4random()) / 0xFFFFFFFF)
        return (rand) * (to - from) + from
    }
    
    func processRawPointData(rawData: String) -> [CGPoint]
    {
        var finalArray = [CGPoint]()
        
        var pointDataAsString = [String]()
        pointDataAsString = rawData.components(separatedBy: " ")
        
        var pointData = [Int]()
        for i in 0..<pointDataAsString.count
        {
            if i % 2 == 0
            {
                // even number
                var point = CGPoint(x: Int(pointDataAsString[i])! - 50, y: 100 - Int(pointDataAsString[i + 1])! - 50)
                point.x = (point.x / 100) * shapeViewSize.width
                point.y = (point.y / 100) * shapeViewSize.width
                finalArray.append(point)
                print(point)
            }
        }
        
        
        return finalArray
    }
}

extension GameScene: SKPhysicsContactDelegate
{
    func didBegin(_ contact: SKPhysicsContact)
    {
        var firstBody: SKPhysicsBody
        var secondBody: SKPhysicsBody
        
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask
        {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        } else {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
        if firstBody.categoryBitMask == ShapeEdgeCategory && secondBody.categoryBitMask == EnemyCategory
        {
        
        }
        
        if firstBody.categoryBitMask == EnemyCategory && secondBody.categoryBitMask == SliceCategory
        {
            if sliceNode != nil
            {
            sliceNode.removeFromParent()
            sliceNode = nil
            }
            isWon = false
            
            gameState.enter(GameOver.self)
        }
    }
}
extension Array where Element:Equatable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()
        
        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }
        
        return result
    }
}
