//
//  LevelCollectionViewCell.swift
//  SlicerUI
//
//  Created by Francis Conlon on 24/01/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import UIKit

class LevelCollectionViewCell: UICollectionViewCell
{
    
    @IBOutlet var levelPreview: UIImageView!
    @IBOutlet var edgeLabel: UILabel!
    @IBOutlet var interiorLabel: UILabel!
    @IBOutlet var completed: UIImageView!

    
    override func awakeFromNib() {
        
    }
}
