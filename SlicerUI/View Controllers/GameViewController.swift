//
//  GameViewController.swift
//  SlicerUI
//
//  Created by Francis Conlon on 11/01/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit
import GoogleMobileAds

class GameViewController: UIViewController, GADInterstitialDelegate {
    
    var levelDataSource: LevelDataSource!
    var currentLevel: NSMutableDictionary!
    var shapeSize: CGSize!
    var interstitial: GADInterstitial!
    var playedLevels = 0
    var colour: UIColor!
    var fileURL: URL!
    var levelIndex: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        interstitial = createAndLoadInterstitial()
        
        
        let scene = GameScene(fileNamed: "GameScene.sks")!
        scene.shapeViewSize = CGSize(width: scene.size.width - 200, height: scene.size.width - 200)
        scene.navController = navigationController
        scene.initialRawPointData = currentLevel["Points"] as! String
        scene.targetShapeArea = currentLevel["Target"] as! Int
        scene.enemies = currentLevel["Enemies"] as! Int
        scene.edgeEnemies = currentLevel["EdgeEnemies"] as! Int
        scene.levelIndex = levelIndex
        scene.backgroundColor = colour
        // Set the scale mode to scale to fit the window
        scene.scaleMode = .aspectFill
        
        let bigView = self.view as! SKView
        bigView.showsPhysics = true
        bigView.presentScene(scene)
        
    }

    func getLevelFor(level: Int) -> NSMutableDictionary
    {
        return levelDataSource.levels[level] as! NSMutableDictionary
    }
    
    func presentAdvert()
    {
        if interstitial.isReady { interstitial.present(fromRootViewController: self)}
    }
    
    func createAndLoadInterstitial() -> GADInterstitial {
        var interstitial = GADInterstitial(adUnitID: "ca-app-pub-3940256099942544/4411468910")
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
        playedLevels = 0
        
    }
    
    func swapToLevel(level: Int)
    {
        let bigView = self.view as! SKView
        
        let newScene = GameScene(fileNamed: "GameScene.sks")!
        newScene.shapeViewSize = CGSize(width: newScene.size.width - 200, height: newScene.size.width - 200)
        newScene.navController = navigationController
        newScene.initialRawPointData = currentLevel["Points"] as! String
        newScene.targetShapeArea = currentLevel["Target"] as! Int
        newScene.enemies = currentLevel["Enemies"] as! Int
        newScene.edgeEnemies = currentLevel["EdgeEnemies"] as! Int
        newScene.isPaused = true
        newScene
        // Set the scale mode to scale to fit the window
        newScene.scaleMode = .aspectFill
        //newScene.size = bigView.scene!.size
        let reveal = SKTransition.flipHorizontal(withDuration: 0.5)
        //scene!.view!.presentScene(newScene, transition: reveal)
        
        print(bigView.scene)
        bigView.presentScene(newScene, transition: reveal)
        print(bigView.scene)
    }
    
    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}
