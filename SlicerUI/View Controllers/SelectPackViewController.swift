//
//  SelectPackViewController.swift
//  SlicerUI
//
//  Created by Francis Conlon on 11/01/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import UIKit
import SpriteKit
import GoogleMobileAds

class SelectPackViewController: UIViewController, UICollectionViewDelegate, GADRewardBasedVideoAdDelegate
{

    
    
    @IBOutlet var backgroundView: SKView!
    
    var bannerView: GADBannerView!
    var packs: NSMutableArray!
    var scene: SKScene!
    
    var fileURL: URL!
    
    @IBOutlet var packsView: UICollectionView!
    let packDataSource = PackDataSource()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        backgroundView.presentScene(scene)
        
        packsView.dataSource = packDataSource
        packsView.delegate = self
        GADRewardBasedVideoAd.sharedInstance().delegate = self
        loadPacks()
        
        
    }
 
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        view.addConstraints(
            [NSLayoutConstraint(item: bannerView,
                                attribute: .bottom,
                                relatedBy: .equal,
                                toItem: bottomLayoutGuide,
                                attribute: .top,
                                multiplier: 1,
                                constant: 0),
             NSLayoutConstraint(item: bannerView,
                                attribute: .centerX,
                                relatedBy: .equal,
                                toItem: view,
                                attribute: .centerX,
                                multiplier: 1,
                                constant: 0)
            ])
    }
    
    func loadPacks()
    {
        packs = NSMutableArray()
        
        /*print(FileManager.default.fileExists(atPath: filePath))
        let data = try! PropertyListSerialization.data(fromPropertyList: packs, format: .xml, options: 0)
        print(data.enumerated())
        print(FileManager.default.removeItem(atPath: filePath))
        print(FileManager.default.createFile(atPath: filePath, contents: data, attributes: nil))*/
        
        let fm = FileManager.default
        guard let directory = fm.urls(for: .libraryDirectory, in: .userDomainMask).first else { return }
        
        let saveURL = directory.appendingPathComponent("Save")
        
        
        fileURL = saveURL.appendingPathComponent("Packs")
        
        if FileManager.default.fileExists(atPath: fileURL.path)
        {
            print("loading from memory")
            packs = NSMutableArray(contentsOfFile: fileURL.path)!
        } else {
            print("starting new save")
            do { try fm.createDirectory(atPath: saveURL.path, withIntermediateDirectories: true, attributes: nil) } catch let error as NSError {
                print("failed to create")
            }
            
            var fileString = "Packs"
            let filePath = Bundle.main.path(forResource: fileString, ofType: "plist")!
            packs = NSMutableArray(contentsOfFile: filePath)!
            packs.write(toFile: fileURL.path, atomically: false)
            
        }
        packDataSource.packs = []
        for i in 0..<packs.count
        {
            let packData = packs[i] as! NSMutableDictionary
            let name = packData["Name"] as! String
            let numberOfLevels = packData["NumberOfLevels"] as! Int
            let locked = packData["Locked"] as! Bool
            
            packDataSource.packs.add(packData)
        }
        
        self.packsView.reloadSections(IndexSet(integer: 0))
    }
    override func viewDidAppear(_ animated: Bool) {
        if SKTAudio.sharedInstance().backgroundMusicPlayer == nil {
            SKTAudio.sharedInstance().playBackgroundMusic("DigitalLemonade.mp3") }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadPacks()
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton)
    {
        SKTAudio.sharedInstance().playSoundEffect("button_press.wav")
        
        let home = navigationController!.children[navigationController!.children.count - 2] as! HomeScreenViewController
        home.bannerView = bannerView
        home.addBannerViewToView(bannerView)
        
        navigationController!.popViewController(animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let pack = packDataSource.packs[indexPath.row] as! NSMutableDictionary
        let locked = pack["Locked"] as! Bool
        
        if locked && GADRewardBasedVideoAd.sharedInstance().isReady == true
        {
            GADRewardBasedVideoAd.sharedInstance().customRewardString = String(indexPath.row)
            SKTAudio.sharedInstance().backgroundMusicPlayer?.pause()
            GADRewardBasedVideoAd.sharedInstance().present(fromRootViewController: self)
            return
        } else if locked {
            return
        }
    
        SKTAudio.sharedInstance().playSoundEffect("button_press.wav")
        if let levelViewController = storyboard?.instantiateViewController(withIdentifier: "SelectLevelViewController") as? SelectLevelViewController
        {
            levelViewController.packType = PackType(rawValue: indexPath.row)
            let pack = packs[indexPath.row] as! NSDictionary
            levelViewController.resourceString = pack["ResourceName"] as! String
            levelViewController.scene = scene
            levelViewController.bannerView = bannerView
            levelViewController.addBannerViewToView(bannerView)
            levelViewController.packTitle = pack["Name"] as! String
            levelViewController.selectLevelLabel.text = pack["Name"] as! String
            levelViewController.packDataSource = packDataSource
            levelViewController.packResourceURL = fileURL
            levelViewController.packIndex = indexPath.row
            
            navigationController?.pushViewController(levelViewController, animated: true)
        }
    }
    
    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd, didRewardUserWith reward: GADAdReward) {
        print("Give them :\(GADRewardBasedVideoAd.sharedInstance().customRewardString)")
        let pack = Int(GADRewardBasedVideoAd.sharedInstance().customRewardString!)!
        let packData = packs[pack] as! NSDictionary
        packData.setValue(false, forKey: "Locked")
        packs[pack] = packData
        packs.write(toFile: fileURL.path, atomically: false)
        self.loadPacks()
        self.packsView.reloadSections(IndexSet(integer: 0))
    }
    
    func rewardBasedVideoAdDidClose(_ rewardBasedVideoAd: GADRewardBasedVideoAd) {
        GADRewardBasedVideoAd.sharedInstance().load(GADRequest(),
                                                    withAdUnitID: "ca-app-pub-3940256099942544/1712485313")
        SKTAudio.sharedInstance().backgroundMusicPlayer?.play()
    }
}
