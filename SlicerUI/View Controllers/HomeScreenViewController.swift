//
//  HomeScreenViewController.swift
//  SlicerUI
//
//  Created by Francis Conlon on 11/01/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import UIKit
import SpriteKit
import GoogleMobileAds

class HomeScreenViewController: UIViewController, GADBannerViewDelegate
{
    
    var bannerView: GADBannerView!
    var interstitial: GADInterstitial!
    
    @IBOutlet var backgroundView: SKView!
    
    var scene: SKScene!
    
    @IBAction func playPressed(_ sender: UIButton)
    {
        SKTAudio.sharedInstance().playSoundEffect("button_press.wav")
        
        
        if let selectPackViewController = storyboard?.instantiateViewController(withIdentifier: "SelectPackViewController") as? SelectPackViewController
        {
            scene.childNode(withName: "Title")!.alpha = 0
            selectPackViewController.scene = scene
            selectPackViewController.bannerView = bannerView
            selectPackViewController.addBannerViewToView(bannerView)
            
            navigationController?.pushViewController(selectPackViewController, animated: true)
        }
    }
    
    override func viewDidLoad() {
        
        
        scene = SKScene(size: backgroundView.frame.size)
        scene.anchorPoint = CGPoint.zero
        backgroundView.presentScene(scene)
        let labelNode = SKNode()
        labelNode.position = CGPoint(x: scene.size.width/2, y: (scene.size.height / 6) * 4)
        labelNode.zPosition = 15
        labelNode.name = "Title"
        scene.addChild(labelNode)
        
        let the = SKLabelNode(fontNamed: "CurseCasualRegular")
        the.text = "The"
        the.fontSize = 50

        labelNode.addChild(the)
        
        
        let cut = SKLabelNode(fontNamed: "CurseCasualRegular")
        cut.text = "Cut"
        cut.fontSize = 70
        cut.position.y = the.position.y + 75
        var posDeg: CGFloat = 8
        var negDeg: CGFloat = -8
        cut.zRotation = posDeg.degreesToRadians()
        labelNode.addChild(cut)
        
        let shape = SKLabelNode(fontNamed: "CurseCasualRegular")
        shape.text = "Shape"
        shape.fontSize = 70
        shape.position.y = the.position.y - 75
        shape.zRotation = negDeg.degreesToRadians()
        labelNode.addChild(shape)
        
        let grow = SKAction.scale(to: 1.1, duration: 0.5)
        grow.timingMode = .easeInEaseOut
        let shrink = SKAction.scale(to: 1, duration: 0.5)
        shrink.timingMode = .easeInEaseOut
        let sequence = SKAction.sequence([grow, shrink])
        let repeatAction = SKAction.repeatForever(sequence)
        labelNode.run(repeatAction)
        
        let rotateOne = SKAction.rotate(toAngle: posDeg.degreesToRadians(), duration: 0.5)
        rotateOne.timingMode = .easeInEaseOut
        let rotateTwo = SKAction.rotate(toAngle: negDeg.degreesToRadians(), duration: 0.5)
        rotateTwo.timingMode = .easeInEaseOut
        let sequenceOne = SKAction.sequence([rotateOne, rotateTwo])
        let sequenceTwo = SKAction.sequence([rotateTwo, rotateOne])
        
        cut.run(SKAction.repeatForever(sequenceTwo))
        shape.run(SKAction.repeatForever(sequenceOne))
        
        scene.backgroundColor = UIColor(red: 0.192, green: 0.588, blue: 0.663, alpha: 1)
        let backgroundEmitter = SKEmitterNode(fileNamed: "BackgroundParticle.sks")!
        backgroundEmitter.position = CGPoint(x: scene.size.width/2, y: 0)
        backgroundEmitter.zPosition = 5
        scene.addChild(backgroundEmitter)
        
        // In this case, we instantiate the banner with desired ad size.
        bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        
        addBannerViewToView(bannerView)
        
        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        bannerView.rootViewController = self
        bannerView.delegate = self
        bannerView.load(GADRequest())
        super.viewDidLoad()
        
    }
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        view.addConstraints(
            [NSLayoutConstraint(item: bannerView,
                                attribute: .bottom,
                                relatedBy: .equal,
                                toItem: bottomLayoutGuide,
                                attribute: .top,
                                multiplier: 1,
                                constant: 0),
             NSLayoutConstraint(item: bannerView,
                                attribute: .centerX,
                                relatedBy: .equal,
                                toItem: view,
                                attribute: .centerX,
                                multiplier: 1,
                                constant: 0)
            ])
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if SKTAudio.sharedInstance().backgroundMusicPlayer == nil {
            SKTAudio.sharedInstance().playBackgroundMusic("DigitalLemonade.mp3") }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        scene.childNode(withName: "Title")?.alpha = 1

    }
    
}
