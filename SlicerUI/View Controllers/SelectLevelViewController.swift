//
//  SelectLevelViewController.swift
//  SlicerUI
//
//  Created by Francis Conlon on 11/01/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import UIKit
import SpriteKit
import GoogleMobileAds

class SelectLevelViewController: UIViewController, UICollectionViewDelegate
{
    var packType: PackType!
    var resourceString: String!
    var packTitle: String!
    var packDataSource: PackDataSource!
    var packResourceURL: URL!
    var packIndex: Int!
    
    @IBOutlet var backgroundView: SKView!
    var scene: SKScene!
    
    var bannerView: GADBannerView!
    
    @IBOutlet var levelView: UICollectionView!
    let levelDataSource = LevelDataSource()
    
    var levelsArray: [NSDictionary]!
    var levels = NSMutableArray()
    
    @IBOutlet var selectLevelLabel: UILabel!
    
    var buttons = [UIButton]()
    
    
    var fileURL: URL!
    
    var completedNo : Int!
    
    override func viewDidLoad()
    {
        //selectLevelLabel.text = pack
        
        backgroundView.presentScene(scene)
        
        levelView.dataSource = levelDataSource
        levelView.delegate = self
        

        
        let fm = FileManager.default
        guard let directory = fm.urls(for: .libraryDirectory, in: .userDomainMask).first else { return }
        
        let saveURL = directory.appendingPathComponent("Save")
        
        
        fileURL = saveURL.appendingPathComponent(resourceString)
        
        loadLevels()
        
        
    }
    
    func loadLevels()
    {
    
        levels = NSMutableArray()
        
        if FileManager.default.fileExists(atPath: fileURL.path)
        {
            print("loading from memory")
            levels = NSMutableArray(contentsOfFile: fileURL.path)!
        } else {
            print("starting new save")
            
            var fileString = resourceString
            print(fileString)
            let filePath = Bundle.main.path(forResource: fileString, ofType: "plist")!
            levels = NSMutableArray(contentsOfFile: filePath)!
            levels.write(toFile: fileURL.path, atomically: false)
            print("loading levels")
            
        }
        
        completedNo = 0
        for i in 0..<levels.count
        {
            let levelData = levels[i] as! NSMutableDictionary
            
            let name = levelData["Name"] as! String
            let points = levelData["Points"] as! String
            let target = levelData["Target"] as! Int
            let enemies = levelData["Enemies"] as! Int
            let edgeEnemies = levelData["EdgeEnemies"] as! Int
            let completed = levelData["Completed"] as! Bool
            
            if completed { completedNo += 1}
           
            
        }
        levelDataSource.levels = levels
        print(completedNo)
        
        self.levelView.reloadSections(IndexSet(integer: 0))
    }
    
    override func viewDidAppear(_ animated: Bool) {

        if SKTAudio.sharedInstance().backgroundMusicPlayer == nil {
            SKTAudio.sharedInstance().playBackgroundMusic("DigitalLemonade.mp3") }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        loadLevels()
        
        let number = packDataSource.packs[packIndex] as! NSMutableDictionary
        let NoOfCo = number["NumberOfCompleted"]
        print(packResourceURL, packDataSource.packs.count, packIndex, NoOfCo, completedNo)
        number.setValue(completedNo, forKey: "NumberOfCompleted")
        packDataSource.packs[packIndex] = number
        packDataSource.packs.write(to: packResourceURL, atomically: true)
    }
    
    override func viewWillLayoutSubviews() {
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton)
    {
        SKTAudio.sharedInstance().playSoundEffect("button_press.wav")
        
        let pack = navigationController!.children[navigationController!.children.count - 2] as! SelectPackViewController
        pack.bannerView = bannerView
        pack.addBannerViewToView(bannerView)
        
        navigationController!.popViewController(animated: true)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        SKTAudio.sharedInstance().playSoundEffect("button_press.wav")
        
        print("Selected: \(indexPath.row)")
        
        if let gameViewController = storyboard?.instantiateViewController(withIdentifier: "GameViewController") as? GameViewController
        {
            gameViewController.currentLevel = levelDataSource.levels[indexPath.row] as! NSMutableDictionary
            gameViewController.levelDataSource = levelDataSource
            gameViewController.colour = scene.backgroundColor
            gameViewController.levelIndex = indexPath.row
            gameViewController.fileURL = fileURL
            
            navigationController?.pushViewController(gameViewController, animated: true)
        }
    }
    
    func addBannerViewToView(_ bannerView: GADBannerView) {
        bannerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(bannerView)
        view.addConstraints(
            [NSLayoutConstraint(item: bannerView,
                                attribute: .bottom,
                                relatedBy: .equal,
                                toItem: bottomLayoutGuide,
                                attribute: .top,
                                multiplier: 1,
                                constant: 0),
             NSLayoutConstraint(item: bannerView,
                                attribute: .centerX,
                                relatedBy: .equal,
                                toItem: view,
                                attribute: .centerX,
                                multiplier: 1,
                                constant: 0)
            ])
    }

}
