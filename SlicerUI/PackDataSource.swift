//
//  PackDataSource.swift
//  SlicerUI
//
//  Created by Francis Conlon on 25/01/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import UIKit

class PackDataSource: NSObject, UICollectionViewDataSource
{
    var packs = NSMutableArray()
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return packs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let identifier = "PackCollectionViewCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)  as! PackCollectionViewCell
        
        let pack = packs[indexPath.row] as! NSMutableDictionary
        cell.nameLabel.text = pack["Name"] as! String
        if pack["Locked"] as! Bool {
            cell.lockedLabel.alpha = 1
            cell.percentageLabel.alpha = 0
        } else {
            cell.lockedLabel.alpha = 0
            cell.percentageLabel.alpha = 1
            let number = pack["NumberOfLevels"] as! Float
            let noc = pack["NumberOfCompleted"] as! Float
            var percent = (noc / number) * 100
            percent.round()
            cell.percentageLabel.text = "\(percent)%"
        }
        
        
        
        return cell
    }
}
