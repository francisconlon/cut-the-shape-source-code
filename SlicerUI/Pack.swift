//
//  Pack.swift
//  SlicerUI
//
//  Created by Francis Conlon on 25/01/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import Foundation

class Pack: NSObject
{
    var name: String
    var numberOfLevels: Int
    var locked: Bool
    
    init(name: String, numberOfLevels: Int, locked: Bool)
    {
        self.name = name
        self.numberOfLevels = numberOfLevels
        self.locked = locked
    }
    
}
